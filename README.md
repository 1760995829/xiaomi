# XiaoMi



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/1760995829/xiaomi.git
git branch -M main
git push -uf origin main
```

## 目录结构
```
project-root/
├── src/
│   ├── main/
│   │   ├── java/
│   │   │   └── com/
│   │   │       └── example/
│   │   │           ├── MiBatteryMonitorApplication.java
│   │   │           ├── cache/
│   │   │           │   └── RedisCache.java
│   │   │           ├── controller/
│   │   │           │   └── WarnController.java
│   │   │           ├── dao/
│   │   │           │   ├── VehicleDAO.java
│   │   │           │   └── WarnRuleDAO.java
│   │   │           ├── dto/
│   │   │           │   ├── WarnRequest.java
│   │   │           │   └── WarnResponse.java
│   │   │           ├── entity/
│   │   │           │   ├── Vehicle.java
│   │   │           │   ├── WarnInfo.java
│   │   │           │   ├── WarnQuery.java
│   │   │           │   └── WarnRule.java
│   │   │           ├── service/
│   │   │           │   ├── VehicleService.java
│   │   │           │   ├── WarnService.java
│   │   │           │   └── impl/
│   │   │           │       ├── VehicleServiceImpl.java
│   │   │           │       └── WarnServiceImpl.java
│   │   │           └── unit/
│   │   │               └── ApplicationContextUtils.java
│   │   ├── resources/
│   │   │   ├── application.yml
│   │   │   └── com/
│   │   │       └── example/
│   │   │           └── mapper/
│   │   │               ├── VehicleDaoMapper.xml
│   │   │               └── WarnRuleDaoMapper.xml
│   ├── test/
│   │   ├── java/
│   │   │   └── com/
│   │   │       └── example/
│   │   │           └── WarnControllerTest.java
│   │   │           └── WarnRuleServiceTest.java
```
